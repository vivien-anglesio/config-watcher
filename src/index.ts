"use strict";

import * as fs from 'fs-extra';
import * as path from 'path';
import * as Chokidar from 'chokidar';
import * as sass from 'node-sass';
import * as upperCamelCase from 'uppercamelcase';
import * as camelCase from 'camelcase';
import * as Bluebird from 'bluebird';
import ConfigRepository from './config-repository';
import * as EventEmitter from 'events';
import {FSWatcher} from "chokidar";
import {Inspection} from "bluebird";
import {ConfigFileData} from "./interfaces";
import {Observable, Observer, Subscription} from 'rxjs';
import Timer = NodeJS.Timer;
import * as terser from 'terser';
import {MinifyOutput} from "terser";

const usePolling = process.env.CLICLIC_CHOKIDAR_MODE === 'polling';
if (usePolling) {
    console.log('USE POLLING MODE IN CHOKIDAR');
}

const terserOptions = {
    parse: {
        bare_returns: true
    },
    compress: {
        dead_code: false,
        drop_debugger: false,
        inline: false,
        reduce_vars: false,
        reduce_funcs: false,
        side_effects: false,
        unused: false,
        negate_iife: false,
    }
}

export interface WatcherConfig {
    configsDirectory: string,
    commitDelay: number
}

const watcherConfig: WatcherConfig = {
    configsDirectory: '',
    commitDelay: 600
};

interface WatcherChanges {
    [index: string]: string
}

module.exports = new EventEmitter();
let watcherSubscription: Subscription;

const watcher$: Observable<WatcherChanges> = new Observable ((subscriber: Observer<WatcherChanges>) => {
    const watcher = Chokidar.watch(watcherConfig.configsDirectory, {usePolling, depth: 4, awaitWriteFinish: {stabilityThreshold: 500, pollInterval: 100}})
    .once('ready', onWatcherReady);

    function onWatcherReady (this: FSWatcher) {
        console.log ('----------------------------------- CONFIGS WATCHER IS READY --------------------------------------------');
        this.on('all', onWatcherEvent)
        .on('error', onWatcherError);
    }

    let changes: WatcherChanges = {};
    let commitTimer: Timer | null = null;
    let lastTs: number;

    function onWatcherEvent(type: string, path: string) {
        if (commitTimer) clearTimeout(commitTimer);
        commitTimer = setTimeout(commitChanges, watcherConfig.commitDelay);
        changes[path]= type;
    }

    function commitChanges () {
        commitTimer = null;
        subscriber.next(changes);
        changes = {};
        if (lastTs) {
            console.log ('Last change commit was ' + (Date.now() - lastTs) + 'ms');
        }
        lastTs = Date.now();
    }

    function onWatcherError(e: Error) {
        console.error (e)
    }

    return function () {
        watcher.close();
        watcher
        .off('ready', onWatcherReady)
        .off('all', onWatcherEvent)
        .off('error', onWatcherError);
    }
});

function watcherEventHandler (changes: WatcherChanges) {
    const actions: any = {};

    for (const eventPath in changes) {
        const type = changes[eventPath];
        const splitted = eventPath.replace (watcherConfig.configsDirectory || '', '').split(path.sep).filter(Boolean);
        const client = splitted[0];

        console.log (eventPath + ' : ' + type);

        if (splitted.length === 1)
        {
            if (type === 'addDir') {
                actions[client] = true;
            } else if (type === 'unlinkDir') {
                actions[client] = false;
            }
        } else if (splitted.length === 2) {
            if (typeof actions[client] === 'boolean') continue;
            const clientActions = actions[client] || (actions[client] = {});
            const config = splitted[1];
            if (type === 'addDir') {
                clientActions[config] = true;
            } else if (type === 'unlinkDir') {
                clientActions[config] = false;
            } else {
                clientActions[config] = type;
            }
        } else if (splitted.length > 2) {
            if (typeof actions[client] === 'boolean') continue;
            const clientActions = actions[client] || (actions[client] = {});
            const config = splitted[1];
            if (typeof clientActions[config] === 'boolean') continue;
            const configActions = clientActions[config] || (clientActions[config] = {});
            const element = splitted[2];
            if (type === 'addDir' || splitted.length > 3) {
                configActions[element] = true;
            } else if (type === 'unlinkDir') {
                configActions[element] = false;
            }
        }
    }

    console.log ('WATCHER APPLY CHANGES : ');
    console.log (actions);

    for (let client in actions) {
        const clientActions = actions[client];
        const normalizedClient = upperCamelCase(client);
        if (typeof clientActions === 'boolean') {
            if (clientActions) {
                console.log ("REPO : "+client+" has been added");
                getRepo(normalizedClient);
                readConfigs(client);
            } else {
                console.log ("REPO : "+client+" has been removed");
                if (repos.has(normalizedClient)) {
                    repos.get(normalizedClient).clear();
                }
            }
        } else {
            for (const config in clientActions) {
                const configActions = clientActions[config];
                const normalizedConfig = camelCase(config);
                if (typeof configActions === 'boolean') {
                    if (configActions) {
                        console.log ("CONFIG : "+client+" > "+config+" has been added");
                        getRepo(normalizedClient).addConfig(normalizedConfig);
                        readElementConfigs(client, config)
                        .map(function (inspection: Inspection<string>) {
                            if (inspection.isRejected()) {
                                console.error ("CONFIG : "+client+" > "+config+" error");
                                console.error (inspection.reason());
                            }
                        });
                    } else {
                        console.log ("CONFIG : "+client+" > "+config+" has been removed");
                        repos.get(normalizedClient).removeConfig(normalizedConfig);
                    }
                } else if (typeof configActions === 'string') {
                    switch (configActions) {
                        case 'unlink':
                            console.log ("CLIENT COMMON FILE : " + client + " > " + config +" has been removed");
                            repos.get(normalizedClient).destroyCommonFile(path.basename(config, path.extname(config)));
                            break;
                        case 'add':
                        case 'change':
                            console.log ("CLIENT COMMON FILE  : " + client + " > " + config +" update");
                            readCommonRepoFiles(client)
                            .catch(function (e: Error) {
                                console.error ("CLIENT COMMON FILE : " + client + " > " + config + " update error");
                                console.error (e);
                            });
                            break;
                    }
                } else {
                    for (const element in configActions) {
                        const elementAction = configActions[element];
                        if (elementAction) {
                            console.log ("ELEMENT CONFIG : "+client+" > "+config+" > "+element+" update");
                            readElementConfig(client, config, element)
                            .catch(function (e: Error) {
                                console.error ("ELEMENT CONFIG : "+client+" > "+config+" > "+element+" update error");
                                console.error (e);
                            });
                        } else {
                            console.log ("ELEMENT CONFIG : "+client+" > "+config+" > "+element+" has been removed");
                            repos.get(normalizedClient).configs.get(normalizedConfig).destroyElement(element);
                        }
                    }
                }
            }
        }
    }


/*
    console.log ("CONFIG WATCHER EVENT : " + event.type, splitted);

    if (splitted.length === 1)
    {
        switch (event.type)
        {
            case 'addDir':
                console.log ("REPO : "+client+" has been added");
                getRepo(normalizedClient);
                readConfigs(client);
                break;
            case 'unlinkDir':
                console.log ("REPO : "+client+" has been removed");
                if (repos.has(normalizedClient))
                {
                    repos.get(normalizedClient).clear();
                }
                break;
        }
    }
    else if (splitted.length === 2)
    {
        let name = splitted[1];
        if (!repos.has(normalizedClient)) return;

        switch (event.type)
        {
            case 'addDir':
                console.log ("CONFIG : "+client+" > "+name+" has been added");
                getRepo(normalizedClient).addConfig(camelCase(name));
                readElementConfigs(client, name)
                .map(function (inspection: Inspection<string>)
                {
                    if (inspection.isRejected()) {
                        console.error ("CONFIG : "+client+" > "+name+" error");
                        console.error (inspection.reason());
                    }
                });
            break;
            case 'unlinkDir':
                console.log ("CONFIG : "+client+" > "+name+" has been removed");
                repos.get(normalizedClient).removeConfig(camelCase(name));
            break;
        }
    }
    else if (splitted.length === 3)
    {
        let name = splitted[1];
        let normalizedName = camelCase(name);
        let elementName = splitted[2];

        if (!repos.has(normalizedClient) || !repos.get(normalizedClient).configs.has(normalizedName)) return;

        switch (event.type)
        {
            case 'addDir':
                console.log ("ELEMENT CONFIG : "+client+" > "+name+" > "+elementName+" has been added");
                readElementConfig(client, name, elementName)
                .catch(function (e: Error)
                {
                    console.error ("ELEMENT CONFIG : "+client+" > "+name+" > "+elementName+" update error");
                    console.error (e);
                });
                break;
            case 'unlinkDir':
                console.log ("ELEMENT CONFIG : "+client+" > "+name+" > "+elementName+" has been removed");
                repos.get(normalizedClient).configs.get(camelCase(name)).destroyElement(elementName);
            break;
        }
    }
    else if (splitted.length > 3)
    {
        let name = splitted[1];
        let normalizedName = camelCase(name);
        let elementName = splitted[2];

        if (!repos.has(normalizedClient) || !repos.get(normalizedClient).configs.has(normalizedName)) return;

        switch (event.type)
        {
            case 'add':
            case 'change':
            case 'unlink':
                console.log ("ELEMENT CONFIG : "+client+" > "+name+" > "+elementName+" has change");
                readElementConfig(client, name, elementName)
                .catch(function (e: Error)
                {
                    console.error ("ELEMENT CONFIG : "+client+" > "+name+" > "+elementName+" update error");
                    console.error (e);
                });
                break;
        }
    }*/
}

module.exports.trace = function () {};

module.exports.config = function (c: Partial<WatcherConfig>)
{
    if (c.configsDirectory) {
        c.configsDirectory = path.join(c.configsDirectory);
        if (c.configsDirectory != watcherConfig.configsDirectory) {
            clearRepos();
            watcherConfig.configsDirectory = c.configsDirectory;
            if (watcherSubscription) {
                watcherSubscription.unsubscribe();
            }

            watcherSubscription = watcher$.subscribe(watcherEventHandler);
        }
    }

    if (c.commitDelay) watcherConfig.commitDelay = c.commitDelay;

    console.log ("set config watcher to directory : "+watcherConfig.configsDirectory);

    return readClients()
    .then(function()
    {
        console.info ("'----------------------------------- CONFIGS LIST --------------------------------------------'");
        for (let repo of repos.values()) {
            console.info(repo.name);
            for (let config of repo.configs.values())
            {
                console.info('    - ', config.name);
            }
        }
        console.info ("'----------------------------------------------------------------------------------------------------'");
    })
    .catch(function (e: Error)
    {
        console.error ("READING CONFIGS FAILED : ");
        console.error (e.stack);
    })

};

module.exports.getRepository = function (client: string)
{
    return getRepo(upperCamelCase(normalize(client)));
};

module.exports.normalizeClientName = function (name: string)
{
    return upperCamelCase(normalize(name));
};

module.exports.get = function (client: string, name: string)
{
    if (typeof client !== 'string') return null;
    if (typeof name !== 'string')
    {
        const splitted = client.split('/');
        client = splitted[0];
        name = splitted[1];
    }
    let uccClient = upperCamelCase(normalize(client));
    if (typeof name !== 'string' || !name)
    {
        console.log('WATCHER TRY GETTING ' + uccClient);
        return getRepo(uccClient);
    }
    else
    {
        let ccName = camelCase(normalize(name));
        console.log ('WATCHER TRY GETTING '+uccClient+ ' > ' + ccName)
        if (repos.has(uccClient))
        {
            if (repos.get(uccClient).configs.has(ccName))
            {
                return repos.get(uccClient).configs.get(ccName);
            }
            else
            {
                console.log ('WATCHER CONFIG NOT FOUND : '+uccClient+ ' > ' + ccName);
            }
        }
        else
        {
            console.log ('WATCHER REPO NOT FOUND : '+uccClient);
        }
    }
    return null;
};

function readElementConfig (client: string, name: string, elementName: string)
{
    let repo = getRepo(upperCamelCase(client));
    let config = repo.addConfig(camelCase(name));

    config.clear(elementName);
    return readFiles(path.join(watcherConfig.configsDirectory, client, name, elementName))
    .map(function(file: string)
    {
        return readConfigFile (path.join(watcherConfig.configsDirectory, client, name, elementName, file))
        .then (function (fileDatas)
        {
            config.update(elementName, fileDatas);
        })
        .catch((e: Error) => {
            console.error (`${client} > ${name} > ${elementName} > ${file} : \n${e.message}`);
        });
    })
    .return(config);
}

function readCommonRepoFile (client: string, file: string)
{
    let repo = getRepo(upperCamelCase(client));
    return Bluebird.resolve(readConfigFile (path.join(watcherConfig.configsDirectory, client, file)))
    .then (function (fileData) {
        repo.commonFileUpdate(fileData);
    })
    .catch((e: Error) => {
        console.error (`${client} > ${file} : \n${e.message}`);
    });
}

function readCommonRepoFiles (client: string)
{
    return readFiles(path.join(watcherConfig.configsDirectory, client))
    .map(function (file: string)
    {
        return readCommonRepoFile(client, file).reflect();
    });
}

function readElementConfigs (client: string, name: string)
{
    return readDirs(path.join(watcherConfig.configsDirectory, client, name))
    .map(function (dir: string)
    {
        return readElementConfig(client, name, dir).reflect();
    });
}

function readConfigs (client: string)
{
    return readDirs(path.join(watcherConfig.configsDirectory, client))
    .map(function (dir: string) {
        return readElementConfigs(client, dir);
    })
    .then(function () {
        return readCommonRepoFiles(client);
    })
}

function readClients ()
{
    return readDirs(watcherConfig.configsDirectory)
    .map(function (dir: string)
    {
        return readConfigs(dir);
    });
}

function readConfigFile (file: string): Promise<ConfigFileData> {
    const ext = path.extname(file);
    const result: ConfigFileData = {
        name: camelCase(path.basename (file, ext)),
        content: ''
    };

    return fs.readFile(file)
    .then(function (data): Promise<ConfigFileData> | ConfigFileData
    {
        switch (ext)
        {
            case '.js':
                return insertTemplates(data.toString(), path.dirname(file))
                .then(function (str)
                {
                    result.exportName = result.name;
                    result.name = 'macros';
                    result.content = str;
                    return result;
                });
            case '.json':
                return insertTemplates(data.toString(), path.dirname(file))
                .then(function (str)
                {
                    result.content = JSON.parse(str);
                    return result;
                });
            case '.scss':
                return new Promise(function (resolve, reject) {
                    sass.render({data: data.toString()}, function (err, sassResult) {
                        if (err) return reject(err);
                        result.content = sassResult.css.toString();
                        result.name = 'styles';
                        resolve(result);
                    })
                });
            default:
                result.content = '';
                return result;
        }
    });
}

interface DirData {
    stat: fs.Stats,
    dir: string
}

interface FileData {
    stat: fs.Stats,
    file: string
}

function readDirs (root: string)
{
    return Bluebird.resolve(fs.readdir (root))
    .map(function (dir: string) {
        return fs.stat(path.join(root,dir))
        .then(function (stat) {
            return {stat: stat, dir: dir};
        });
    })
    .filter(function (dirData: DirData) {
        return dirData.stat.isDirectory();
    })
    .map(function (dirData: DirData)
    {
        return dirData.dir;
    });
}

function readFiles (root: string) {
    return Bluebird.resolve(fs.readdir (root))
    .map(function (file: string) {
        return fs.stat(path.join(root, file))
        .then(function (stat)
        {
            return {stat: stat, file: file};
        });
    })
    .filter(function (fileData: FileData)
    {
        return fileData.stat.isFile();
    })
    .map(function (fileData: FileData)
    {
        return fileData.file;
    });
}

function normalize (name: string) {
    return name.replace(/([A-Z0-9]+)/g, function($1){return "-"+$1.toLowerCase()+($1.length > 1 ? "-":"");}).replace(/^-/, "").replace(/-$/, "").replace(/[\/\\?%\*:|"<> ]/g, "-");
}

const jsRegExp = /\{\{ ?JS ?: ?(.+?)(?: ?: ?(.+?))?\}\}/ig;
const htmlTplRegExp = /: ?"(.+?\.html?)"/ig;

function insertTemplates (str: string, dirname: string)
{
    const fileReadCache = new Map();
    let templates = [];
    let match;
    while (match = htmlTplRegExp.exec(str))
    {
        const context = {
            match: match[0] && match[0].trim(),
            pathLog: match[1],
            path: path.join(dirname, match[1] && match[1].trim()),
            index: match.index,
            content: ": ''"
        };
        let p = fileReadCache.get(context.path);
        if (!p) {
            p = fs.readFile(context.path);
            fileReadCache.set(context.path, p);
        }
        templates.push(p
            .then(function(data: string)
            {
                context.content = ': ' + JSON.stringify(data.toString());
            })
            .catch(function ()
            {
                console.error ('CONFIG : template file not found '+context.pathLog);
            })
            .then(function()
            {
                return context;
            })
        );
    }


    const fnCache = new Map();
    while (match = jsRegExp.exec(str))
    {
        const context = {
            match: match[0] && match[0].trim(),
            pathLog: match[1],
            path: path.join(dirname, match[1] && match[1].trim()),
            fnName: match[2] && match[2].trim(),
            index: match.index,
            content: '',
            removeWrappingQuotes: false,
        };
        let p = fileReadCache.get(context.path);
        if (!p) {
            p = fs.readFile(context.path)
            .then(function (data) {
                return insertTemplates(data.toString(), dirname);
            }).then(function (content) {
                return terser.minify(content, terserOptions);
            })
            fileReadCache.set(context.path, p);
        }
        templates.push(
            p
            .then(async function(minResult: terser.MinifyOutput)
            {
                if (minResult.code) {
                    if (context.fnName) {
                        try
                        {
                            const fn = parseFnObject(minResult.code, context.fnName);
                            context.content = JSON.stringify(fn);
                            context.removeWrappingQuotes = true;
                        } catch (e: any) {
                            console.error ('CONFIG : cannot parse js function ' + context.pathLog, e.message);
                        }
                    } else {
                        try
                        {
                            const code = minResult.code || '';
                            context.content = JSON.stringify(code).slice(1, -1);
                        } catch (e: any) {
                            console.error ('CONFIG : cannot minify js code ' + context.pathLog, e.message);
                        }
                    }
                }
            })
            .catch(function (e: any)
            {
                console.error(e);
                console.error ('CONFIG : template file not found ' + context.pathLog);
            })
            .then(function()
            {
                return context;
            })
        );
    }

    return Promise.all(templates)
    .then(function (templates)
    {
        for (let i = templates.length-1; i>=0; i--) {
            const template = templates[i];
            if (template.removeWrappingQuotes && str[template.index - 1] === '"' && str[template.index + template.match.length] === '"') {
                str = replaceBetween(str, template.content, template.index - 1, template.match.length + 2);
            } else {
                str = replaceBetween(str, template.content, template.index, template.match.length);
            }
        }
        return str;
    });
}
//
// function includeExternalJS (str: string, dirname: string, fileReadCache: Map<any, any>) {
//     const match = jsRegExp.exec(str)
//     if (match) {
//         const context = {
//             match: match[0] && match[0].trim(),
//             pathLog: match[1],
//             path: path.join(dirname, match[1] && match[1].trim()),
//             fnName: match[2] && match[2].trim(),
//             index: match.index,
//             content: '',
//             removeWrappingQuotes: false,
//         };
//         let p = fileReadCache.get(context.path);
//         if (!p) {
//             p = fs.readFile(context.path)
//             .then(function (data) {
//                 const content = data.toString();
//                 while (includeExternalJS(str)) {
//
//                 }
//             })
//             .then(function(content) {
//                 return terser.minify(content, terserOptions);
//             })
//             fileReadCache.set(context.path, p);
//         }
//         return p;
//     }
// }

function replaceBetween (ipt: string, sub: string, start: number, len: number)
{
    return ipt.substring(0, start) + sub + ipt.substring(start+len);
}

var repos = new Map();

function getRepo (name: string)
{
    if (repos.has(name)) return repos.get(name);
    let repo = new ConfigRepository(name);
    repos.set(name, repo);
    module.exports.emit('repository-created', repo);
    module.exports.emit('repository-created:' + name, repo);
    return repo;
}

function clearRepos ()
{
    repos.forEach((client, repo) =>
    {
        repo.destroy();
    });
    repos.clear();
}

function parseFnObject(str: string, fnName: string) {
    const fnRegExp = new RegExp(`function\\s+${fnName}\\s*\\(`);
    const m = str.match(fnRegExp);
    if (m && typeof m.index === 'number') {
        let i = m.index + m[0].length;
        let openedBrackets = 0;
        let phase = 'args';
        let sub = '';
        const result: string[] = [];
        let endOfArg = false;
        while (i < str.length) {
            switch (phase) {
                case 'args':
                    if (str[i] === ',') {
                        endOfArg = true;
                    } else if (str[i] === ')') {
                        endOfArg = true;
                        phase = 'body';
                    }
                    if (endOfArg) {
                        endOfArg = false;
                        if (sub) {
                            sub = sub.trim();
                            if (sub) {
                                result.push(sub);
                            }
                        }
                        sub = '';
                    } else {
                        sub += str[i];
                    }
                break;
                case 'body':
                    switch (str[i]) {
                        case '{':
                            openedBrackets++;
                            if (openedBrackets > 1) {
                                sub += str[i];
                            }
                        break;
                        case '}':
                            openedBrackets--;
                            if (openedBrackets === 0) {
                                result.push(sub);
                                return result;
                            } else {
                                sub += str[i];
                            }
                            break;
                        default:
                            sub += str[i];
                    }
            }
            i++
        }
    }
    throw new Error('Cannot parse function ' + fnName);
}
