import {EventEmitter} from 'events';
import * as helpers from './helpers';
import Config from './config';
import {ListenerMap} from "./helpers";

export default class ConfigRepository extends EventEmitter
{

    name: string;
    configs: Map<string, Config> = new Map();
    commonFiles: Map<string, any> = new Map();

    private _configListeners: ListenerMap;

    constructor(name: string)
    {
        super();
        this.name = name;
        this._configListeners = {
            'destroyed': this._onConfigDestroyed.bind(this),
            'element-destroyed': this._onConfigElementDestroyed.bind(this)
        }
    }

    private _onConfigDestroyed(config: Config)
    {
        this.configs.delete(config.name);
        this.emit('config-destroyed', config, this);
    }

    private _onConfigElementDestroyed(config: Config, elementName: string)
    {
        this.emit('config-element-destroyed', config, elementName, this);
    }

    addConfig(name: string)
    {
        if (!this.configs.has(name))
        {
            console.log ('Add config : ' + name + ' to ' + this.name);
            let config = new Config(name, this);
            helpers.addListenersTo(config, this._configListeners);
            this.configs.set(name, config);
            this.emit ('config-created', config, this);
            return config;
        }
        return this.configs.get(name);
    }

    removeConfig(name: string)
    {
        const config = this.configs.get(name);
        if (config) {
            config.destroy();
        } else {
            console.log ('try to remove non existing config '+name+ ' on '+this.name);
            console.log (this.configs.keys());
        }
    }

    commonFileUpdate(data: any) {
        this.commonFiles.set(data.name, data.content);
        this.emit ('common-file-updated', data.name, data.content, this)
    }

    destroyCommonFile(name: string) {
        this.commonFiles.delete(name);
        this.emit ('common-file-destroyed', name, this);
    }

    clear()
    {
        this.configs.forEach((config) => config.destroy());
    }

    destroy()
    {
        this.emit('destroy', this);
        this.clear();
        this.emit('destroyed', this);
        this.removeAllListeners();
    }
}
