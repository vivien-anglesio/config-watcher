import {EventEmitter} from "events";

export declare interface ListenerMap {
    [index: string]: (...args: any) => void;
}

export function replaceBetween (ipt: string, sub: string, start: number, len: number)
{
    return ipt.substring(0, start) + sub + ipt.substring(start+len);
}

export function normalize (name: string)
{
    return name.replace(/([A-Z0-9]+)/g, function($1){return "-"+$1.toLowerCase()+($1.length > 1 ? "-":"");}).replace(/^-/, "").replace(/-$/, "").replace(/[\/\\?%\*:|"<> ]/g, "-");
}

export function addListenersTo(target: EventEmitter, listeners: ListenerMap)
{
    for (var i in listeners)
    {
        target.on(i,listeners[i]);
    }
    return target;
}

export function removeListenersFrom(target: EventEmitter, listeners: ListenerMap)
{
    for (var i in listeners)
    {
        target.removeListener(i,listeners[i]);
    }
    return target;
}
