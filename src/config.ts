import {EventEmitter} from "events";
import ConfigRepository from "./config-repository";
import {ConfigFileData} from "./interfaces";

interface ConfigElementMap
{
    [index: string]: ConfigElement
}

type ConfigElement = {
    macros?: {
        [index: string]: string
    }
} & {
    [index: string]: string
}

export default class Config extends EventEmitter {

    name: string;
    id: string;
    repositoryName: string;
    elements: ConfigElementMap;

    constructor(name: string, repo: ConfigRepository)
    {
        super();
        this.elements = {};
        this.name = name;
        this.repositoryName = repo.name;
        this.id = this.repositoryName + '/' + this.name;
    }

    clear(elementConfigName: string)
    {
        delete this.elements[elementConfigName];
    }

    update(elementConfigName: string, datas: ConfigFileData)
    {
        if (!datas.content) return;
        if (!this.elements[elementConfigName]) this.elements[elementConfigName] = {};
        const element = this.elements[elementConfigName];
        switch (datas.name)
        {
            case 'macros':
                if (!element.macros) element.macros = {};
                element.macros[datas.exportName!] = datas.content;
            break;
            default:
                element[datas.name] = datas.content;
        }
        this.emit('updated', this, elementConfigName, datas.name, datas.content);
    }

    destroyElement(elementConfigName: string)
    {
        if (!this.elements[elementConfigName]) return;
        delete this.elements[elementConfigName];
        this.emit('element-destroyed', this, elementConfigName);
    }

    destroy()
    {
        this.emit('destroyed', this);
        this.removeAllListeners();
    }
}
