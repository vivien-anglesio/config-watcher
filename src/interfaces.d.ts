export declare interface ConfigFileData {
    name: string,
    content: string
    exportName?: string
}
