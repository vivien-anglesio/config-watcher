"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
const helpers = require("./helpers");
const config_1 = require("./config");
class ConfigRepository extends events_1.EventEmitter {
    constructor(name) {
        super();
        this.configs = new Map();
        this.commonFiles = new Map();
        this.name = name;
        this._configListeners = {
            'destroyed': this._onConfigDestroyed.bind(this),
            'element-destroyed': this._onConfigElementDestroyed.bind(this)
        };
    }
    _onConfigDestroyed(config) {
        this.configs.delete(config.name);
        this.emit('config-destroyed', config, this);
    }
    _onConfigElementDestroyed(config, elementName) {
        this.emit('config-element-destroyed', config, elementName, this);
    }
    addConfig(name) {
        if (!this.configs.has(name)) {
            console.log('Add config : ' + name + ' to ' + this.name);
            let config = new config_1.default(name, this);
            helpers.addListenersTo(config, this._configListeners);
            this.configs.set(name, config);
            this.emit('config-created', config, this);
            return config;
        }
        return this.configs.get(name);
    }
    removeConfig(name) {
        const config = this.configs.get(name);
        if (config) {
            config.destroy();
        }
        else {
            console.log('try to remove non existing config ' + name + ' on ' + this.name);
            console.log(this.configs.keys());
        }
    }
    commonFileUpdate(data) {
        this.commonFiles.set(data.name, data.content);
        this.emit('common-file-updated', data.name, data.content, this);
    }
    destroyCommonFile(name) {
        this.commonFiles.delete(name);
        this.emit('common-file-destroyed', name, this);
    }
    clear() {
        this.configs.forEach((config) => config.destroy());
    }
    destroy() {
        this.emit('destroy', this);
        this.clear();
        this.emit('destroyed', this);
        this.removeAllListeners();
    }
}
exports.default = ConfigRepository;
