"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
class Config extends events_1.EventEmitter {
    constructor(name, repo) {
        super();
        this.elements = {};
        this.name = name;
        this.repositoryName = repo.name;
        this.id = this.repositoryName + '/' + this.name;
    }
    clear(elementConfigName) {
        delete this.elements[elementConfigName];
    }
    update(elementConfigName, datas) {
        if (!datas.content)
            return;
        if (!this.elements[elementConfigName])
            this.elements[elementConfigName] = {};
        const element = this.elements[elementConfigName];
        switch (datas.name) {
            case 'macros':
                if (!element.macros)
                    element.macros = {};
                element.macros[datas.exportName] = datas.content;
                break;
            default:
                element[datas.name] = datas.content;
        }
        this.emit('updated', this, elementConfigName, datas.name, datas.content);
    }
    destroyElement(elementConfigName) {
        if (!this.elements[elementConfigName])
            return;
        delete this.elements[elementConfigName];
        this.emit('element-destroyed', this, elementConfigName);
    }
    destroy() {
        this.emit('destroyed', this);
        this.removeAllListeners();
    }
}
exports.default = Config;
