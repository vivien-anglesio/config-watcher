"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.removeListenersFrom = exports.addListenersTo = exports.normalize = exports.replaceBetween = void 0;
function replaceBetween(ipt, sub, start, len) {
    return ipt.substring(0, start) + sub + ipt.substring(start + len);
}
exports.replaceBetween = replaceBetween;
function normalize(name) {
    return name.replace(/([A-Z0-9]+)/g, function ($1) { return "-" + $1.toLowerCase() + ($1.length > 1 ? "-" : ""); }).replace(/^-/, "").replace(/-$/, "").replace(/[\/\\?%\*:|"<> ]/g, "-");
}
exports.normalize = normalize;
function addListenersTo(target, listeners) {
    for (var i in listeners) {
        target.on(i, listeners[i]);
    }
    return target;
}
exports.addListenersTo = addListenersTo;
function removeListenersFrom(target, listeners) {
    for (var i in listeners) {
        target.removeListener(i, listeners[i]);
    }
    return target;
}
exports.removeListenersFrom = removeListenersFrom;
